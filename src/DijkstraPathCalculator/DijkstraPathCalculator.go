package DijkstraPathCalculator

import (
	. "MinHeap"
	"log"
)

type UUID uint64
type Cost uint32

type Node interface {
	GetNeighbours() ([]Node, []Cost)
	GetUUID() UUID
}

type Vertice struct {
	node    Node
	cost    Cost
	parent  *Vertice
	visited bool
}

func (v *Vertice) GetValue() uint32 {
	return uint32(v.cost)
}

func (v *Vertice) GetData() interface{} {
	return (v.node)
}

func CalculatePath(source Node, destination Node, exempt_nodes map[UUID]*struct{}) ([]Node, Cost) {
	var destination_to_source []Node
	var cost_of_path Cost
	cost_of_path = 0
	
	// Sanity checking
	// 1) source or destination nodes cannot be in the exempt list
	exempt := exempt_nodes[source.GetUUID()]
	if exempt != nil {
	    log.Println("Source cannot be in exempt list")
	    return destination_to_source, cost_of_path
	}
	exempt = exempt_nodes[destination.GetUUID()]
	if exempt != nil {
	    log.Println("Destination cannot be in exempt list")
	    return destination_to_source, cost_of_path
	}

	min_heap := NewMinHeap()
	var all_vertices map[UUID]*Vertice
	all_vertices = make(map[UUID]*Vertice)

	current_vertice := &Vertice{node: source, cost: 0, parent: nil, visited: true}
	all_vertices[current_vertice.node.GetUUID()] = current_vertice

	for current_vertice != nil && current_vertice.node.GetUUID() != destination.GetUUID() {
		// TODO: Make this fmt a log and make it optional. Same with all other fmt.
		log.Printf("Retrieving neighbours for UUID: %v\n", current_vertice.node.GetUUID())
		nodes, costs := current_vertice.node.GetNeighbours()

		for node_idx := range nodes {
		    exempt = exempt_nodes[nodes[node_idx].GetUUID()]
		    if exempt != nil {
    		    // This node is exempt. Do not bother to calculate or
    		    // add to the minimum heap for later calculations.
    		    continue    
		    }
		    
			neighbour_vertice := all_vertices[nodes[node_idx].GetUUID()]
			if neighbour_vertice == nil {
				neighbour_vertice = &Vertice{node: nodes[node_idx], cost: 0, visited: false}
				all_vertices[neighbour_vertice.node.GetUUID()] = neighbour_vertice
			}

			visited_cost := current_vertice.cost + costs[node_idx]

			if neighbour_vertice.visited == false && (neighbour_vertice.cost == 0 || visited_cost < neighbour_vertice.cost) {
				// TODO: Make this fmt a log and make it optional.
				log.Printf("Neighbour %v Cost Update: %v/%v\n", neighbour_vertice.node.GetUUID(), neighbour_vertice.cost, visited_cost)
				neighbour_vertice.cost = visited_cost
				neighbour_vertice.parent = current_vertice
				min_heap.Push(neighbour_vertice)
			}
		}

		// Dijkstra's algorithm: do not recalculate a node
		// that has already been calculated. If you do, you'll get loops.
		current_vertice.visited = true

        // Get the next item from the min-heap, this will
        // become the current vertice to calculate.
		next_vertice := min_heap.Pop()
		current_vertice = nil

		for next_vertice != nil {
		    current_vertice = next_vertice.(*Vertice)
			if Cost(current_vertice.GetValue()) == current_vertice.cost {
				break
			}
			// else {}
			// If the code flow has made it this far in the loop,
			// this is a duplicate min heap entry. The node
			// has been recalculated via a neighbour and this node
			// now has a larger value on the min heap (key) than
			// what its latest cost calculation is. As an implementation
			// optimization, skip this node and get the next one.
			// There isn't a space-cost effective way to predict this,
			// just use the CPU to compensate when an entry is found.
			// Penalty is O(logn) for every miss multiplied by the
			// number of connected interfaces on the node that qualified
			// for this penalty (rare).
			next_vertice = min_heap.Pop()
		}

	}

	cost_of_path = 0
	if current_vertice != nil {
		cost_of_path = current_vertice.cost
	}

	for current_vertice != nil {
		destination_to_source = append(destination_to_source, current_vertice.node)
		current_vertice = current_vertice.parent
	}

	return destination_to_source, cost_of_path
}
