package DijkstraPathCalculator

import (
	"encoding/json"
	"testing"
)

// A basic node implementation used to test the network.
// This struct is to be replaced by the real nodes
// describing the actual network.
type NodeImpl struct {
	uuid       UUID
	neighbours []Node
	cost       []Cost
}

// Required by the path calculating interface.
// The number of []Nodes must match the number of []Costs.
func (n *NodeImpl) GetNeighbours() ([]Node, []Cost) {
	return n.neighbours, n.cost
}

// Required by the path calculating interface.
// Return the UUID of this node, must be unique amongst all nodes.
func (n *NodeImpl) GetUUID() UUID {
	return (n.uuid)
}

// A JSON structure to be populated which will be translated
// into creating the objects that are the Nodes.
type NodeJSON struct {
	UUID            UUID
	Neighbours_uuid []UUID
	Neighbours_cost []Cost
}

/*
A network generator. Generates a series of connected nodes
based off of JSON data. To understand the value of this function,
see TestBasicPath, which does not use this function.
Edge connections established by this function are considered
bidirectional and equal.

Input: JSON data in []bytes.
Output: A map of UUIDs with NodeImpl as the data.
EG with the following format:
	node_json_data := []byte(`[
	{"UUID":0,"Neighbours_uuid":[1,2],"Neighbours_cost":[15,20]},
	{"UUID":1,"Neighbours_uuid":[2],"Neighbours_cost":[55]}]`)
This can be interpreted as:
1) Node with UUID 0 is connected to nodes with UUIDs of 1 and 2,
the connection (edge) cost is 15 and 20 respectively.
2) Node with UUID 1 is connected to a node with a UUID of 2. The
edge connection (edge) cost is 55.
3) There does not need to be an explicit UUID of 2 declared since its
existence and connection costs are implied. The UUID of 2 will be
created automatically.

Additional Information:
1) UUIDs do not need to be ordered, for the sake of the above example
they were ordered and ascending but do not need to be.
2) In the above example, should the following be added:
{"UUID":2,"Neighbours_uuid":[0,1],"Neighbours_cost":[65,70]}
This will create additional bidirectional connections to nodes with
UUIDs of 0 and 1 with a cost of 65 and 70 respectively. This is equivalent
to having multiple interfaces or links established between two nodes.
3) The below function can be re-cycled for practical use outside of testing.
The return type will need to change and some minor updates to the function
may be necessary to reflect this.
4) The function assumes that the number of UUIDs as input must match the
number of cost input to maintain synchronicity.
*/
func GenerateNodes(node_json_data []byte) map[UUID]*NodeImpl {
	// Unmarshal the JSON expression into a middle-man
	// structure that will later be used to describe/create the
	// Node object.
	var node_json []NodeJSON
	json.Unmarshal(node_json_data, &node_json)

	// Create a map that will be used to return the entire network
	// of assembled Nodes.
	var uuid_to_node map[UUID]*NodeImpl
	uuid_to_node = make(map[UUID]*NodeImpl)

	// For each UUID found in the top level of the JSON data
	// (as described as UUID:#), loop through each UUID and connect
	// it to its neighbour(s) (if any).
	// If the neighbour does not exist then create it.
	// Each current node will be connected to all of its neighbours
	// and each neighbour will be connected back using the same cost.
	for i := 0; i < len(node_json); i++ {
		// Extract the current node data as described in the JSON object.
		current_uuid := node_json[i].UUID
		current_node := uuid_to_node[current_uuid]

		// Create the node if it hasn't been instantiated.
		// Use the data described in the JSON object to fill in the UUID
		// and other potential details.
		if current_node == nil {
			current_node = &NodeImpl{}
			current_node.uuid = current_uuid
			uuid_to_node[current_uuid] = current_node
		}

		// Loop through all of the neighbours of the current node
		// and connect the current node to them as well as
		// connect the neighbours back to the current node.
		for k := 0; k < len(node_json[i].Neighbours_uuid); k++ {
			// Extract the neighbour data as described in the JSON object.
			neighbour_uuid := node_json[i].Neighbours_uuid[k]
			neighbour_cost := node_json[i].Neighbours_cost[k]
			neighbour_node := uuid_to_node[neighbour_uuid]

			// Create the node if it hasn't been instantiated.
			// Use the data described in the JSON object to fill in the UUID
			// and other potential details.
			if neighbour_node == nil {
				neighbour_node = &NodeImpl{}
				neighbour_node.uuid = neighbour_uuid
				uuid_to_node[neighbour_uuid] = neighbour_node
			}
			// else {}
			// Multiple connections back are allowed, hence the commented out else{}.
			// The multiple connections will represent multiple interfaces/links connected
			// to the same boxes. Dijkstra will choose the connection with the
			// lowest cost.

			// Associated the current node with its neighbour nodes by
			// adding the neighbour to the list of connected nodes.
			// Also add the cost for each connection, the index
			// of the cost should be synchronous with the index of the neighbour node.
			current_node.neighbours = append(current_node.neighbours, neighbour_node)
			current_node.cost = append(current_node.cost, neighbour_cost)

			// Exact same reasoning as above but associating the neighbour with
			// the current node. This is what allows for bidirectional
			// path finding. Note that if multiple connections/associations are
			// established then this implies that there are multiple interface/link
			// connections between these nodes, this is supported and it will
			// be used in determining the lowest cost connection.
			neighbour_node.neighbours = append(neighbour_node.neighbours, current_node)
			neighbour_node.cost = append(neighbour_node.cost, neighbour_cost)
		}
	}

	return uuid_to_node
}

// A helper const for the first test case.
const (
	NODE_SOURCE      int = 0
	NODE_A           int = 1
	NODE_B           int = 2
	NODE_C           int = 3
	NODE_D           int = 4
	NODE_E           int = 5
	NODE_DESTINATION int = 6
)

/*
A basic path test:
S-1-A-3--D-5-E-6-D
 \       |
  \2     |1
   B--4--C
*/
func TestBasicPath(t *testing.T) {
	// The below procedures demonstrates a painful, manual method
	// of establishing a network of connected nodes. There
	// is a function to do this automatically that's described elsewhere
	// in this file, use that instead. The purpose of this test
	// is to validate the basic functionality of linking nodes and
	// printing the calculated path without using an auto-assembled network.
	var nodes [7]NodeImpl

	for idx := range nodes {
		nodes[idx].uuid = UUID(idx)
	}

	nodes[NODE_SOURCE].cost = append(nodes[NODE_SOURCE].cost, 1)
	nodes[NODE_SOURCE].cost = append(nodes[NODE_SOURCE].cost, 2)
	nodes[NODE_SOURCE].neighbours = append(nodes[NODE_SOURCE].neighbours, &nodes[NODE_A])
	nodes[NODE_SOURCE].neighbours = append(nodes[NODE_SOURCE].neighbours, &nodes[NODE_B])

	nodes[NODE_A].cost = append(nodes[NODE_A].cost, 1)
	nodes[NODE_A].cost = append(nodes[NODE_A].cost, 3)
	nodes[NODE_A].neighbours = append(nodes[NODE_A].neighbours, &nodes[NODE_SOURCE])
	nodes[NODE_A].neighbours = append(nodes[NODE_A].neighbours, &nodes[NODE_D])

	nodes[NODE_B].cost = append(nodes[NODE_B].cost, 2)
	nodes[NODE_B].cost = append(nodes[NODE_B].cost, 4)
	nodes[NODE_B].neighbours = append(nodes[NODE_B].neighbours, &nodes[NODE_SOURCE])
	nodes[NODE_B].neighbours = append(nodes[NODE_B].neighbours, &nodes[NODE_C])

	nodes[NODE_C].cost = append(nodes[NODE_C].cost, 4)
	nodes[NODE_C].cost = append(nodes[NODE_C].cost, 1)
	nodes[NODE_C].neighbours = append(nodes[NODE_C].neighbours, &nodes[NODE_B])
	nodes[NODE_C].neighbours = append(nodes[NODE_C].neighbours, &nodes[NODE_D])

	nodes[NODE_D].cost = append(nodes[NODE_D].cost, 3)
	nodes[NODE_D].cost = append(nodes[NODE_D].cost, 1)
	nodes[NODE_D].cost = append(nodes[NODE_D].cost, 5)
	nodes[NODE_D].neighbours = append(nodes[NODE_D].neighbours, &nodes[NODE_A])
	nodes[NODE_D].neighbours = append(nodes[NODE_D].neighbours, &nodes[NODE_C])
	nodes[NODE_D].neighbours = append(nodes[NODE_D].neighbours, &nodes[NODE_E])

	nodes[NODE_E].cost = append(nodes[NODE_E].cost, 5)
	nodes[NODE_E].cost = append(nodes[NODE_E].cost, 6)
	nodes[NODE_E].neighbours = append(nodes[NODE_E].neighbours, &nodes[NODE_D])
	nodes[NODE_E].neighbours = append(nodes[NODE_E].neighbours, &nodes[NODE_DESTINATION])

	nodes[NODE_DESTINATION].cost = append(nodes[NODE_DESTINATION].cost, 6)
	nodes[NODE_DESTINATION].neighbours = append(nodes[NODE_DESTINATION].neighbours, &nodes[NODE_D])
	// If you read the above code, you should be a bit dizzy by this point.
	// There is an auto assembler built to help with the above craziness,
	// it will be used from now-on.

	paths, cost := CalculatePath(&nodes[NODE_SOURCE], &nodes[NODE_DESTINATION], nil)

	// Hardcoded answer:
	var path_answer []UUID = []UUID{6, 5, 4, 1, 0}
	var cost_answer Cost = 15
	// Print the path from destination to source.
	for path_idx := range paths {
		t.Logf("UUID: %v\n", paths[path_idx].GetUUID())
		if path_answer[path_idx] != paths[path_idx].GetUUID() {
			t.Logf("Path does not match expected:\nWant:\t%v\nGot:\t%v\nAt index:%v\n",
				path_answer[path_idx], paths[path_idx].GetUUID(), path_idx)
			t.Fail()
			break
		}
	}

	// Print the total cost of the path found.
	t.Logf("Path cost: %v\n", cost)
	if cost != cost_answer {
		t.Logf("Cost does not match expected answer. Expected/Given: %v/%v",
			cost_answer, cost)
		t.Fail()
	}
}

/*
A path test that uses the network assembler based off
of what will be described in the JSON []byte data.
Legend:
Cost between nodes: (#)
Node UUIDs: #
Network to be described and calculated:
       1--(3)--2--(1)--3
   (4)/ \               \(2)
     /   \(2)            \
    0--(7)4--(1)--5---(1)-9
     \   /(4)            /
   (5)\ /               /(2)
       6--(2)--7--(2)--8
*/
func TestAutoPath(t *testing.T) {
	// Reminder: UUIDs do not need to be ordered, it's just done
	// here for readability purposes.
	node_json_data := []byte(`[
	{"UUID":0,"Neighbours_uuid":[1,4,6],"Neighbours_cost":[4,7,5]},
	{"UUID":1,"Neighbours_uuid":[2,4],"Neighbours_cost":[3,2]},
	{"UUID":2,"Neighbours_uuid":[3],"Neighbours_cost":[1]},
	{"UUID":3,"Neighbours_uuid":[9],"Neighbours_cost":[2]},
	{"UUID":4,"Neighbours_uuid":[5,6],"Neighbours_cost":[1,4]},
	{"UUID":5,"Neighbours_uuid":[9],"Neighbours_cost":[1]},
	{"UUID":6,"Neighbours_uuid":[7],"Neighbours_cost":[2]},
	{"UUID":7,"Neighbours_uuid":[8],"Neighbours_cost":[2]},
	{"UUID":8,"Neighbours_uuid":[9],"Neighbours_cost":[2]}
	]`)

	nodes := GenerateNodes(node_json_data)

	// Used for debugging purposes, should something mess up.
	// There will be pointer addresses as the output, these don't
	// need to be decoded - they just need to match the given
	// address of the node they are pointing to (look for a pattern).
	for key, value := range nodes {
		t.Logf("Neighbour map key/value: %v/%v", key, *value)
	}

	// Calculate the best path between UUIDs 0 and 9
	paths, cost := CalculatePath(nodes[0], nodes[9], nil)
	// Print the path using the UUIDs from destination to source.
	for path_idx := range paths {
		t.Logf("Path: %v\n", paths[path_idx].GetUUID())
	}
	// Print the final cost of the path.
	t.Logf("Path cost: %v\n", cost)

	// Calculate the best path between UUIDs 6 and 3.
	paths, cost = CalculatePath(nodes[6], nodes[3], nil)
	for path_idx := range paths {
		t.Logf("Path: %v\n", paths[path_idx].GetUUID())
	}
	t.Logf("Path cost: %v\n", cost)

	// Calculate the best path between UUIDs 3 and 6.
	// Note that it doesn't have to match the above
	// path calculation, the only requirement is that
	// it matches the cost exactly.
	// Note: This is the first real test, all other tests
	// above are used for debugging output.
	compare_cost := cost // Previous cost.
	paths, cost = CalculatePath(nodes[3], nodes[6], nil)
	for path_idx := range paths {
		t.Logf("Path: %v\n", paths[path_idx].GetUUID())
	}
	t.Logf("Path cost: %v\n", cost)
	if compare_cost != cost {
		t.Logf("Cost of complementary paths do not match: %v/%v",
			compare_cost, cost)
		t.Fail()
	}

	// Exempt nodes 5 and 8. This will force
	// the path 0-1-2-3-9 to become the best path.
	// The expected cost is 10.
	// Hardcoded answer:
	var path_answer []UUID = []UUID{9, 3, 2, 1, 0}
	var cost_answer Cost = 10
	var exempt_uuid map[UUID]*struct{} = map[UUID]*struct{}{
		5: &struct{}{},
		8: &struct{}{},
	}
	paths, cost = CalculatePath(nodes[0], nodes[9], exempt_uuid)
	// Print the path from destination to source.
	for path_idx := range paths {
		t.Logf("UUID: %v\n", paths[path_idx].GetUUID())
		if path_answer[path_idx] != paths[path_idx].GetUUID() {
			t.Logf("Path does not match expected:\nWant:\t%v\nGot:\t%v\nAt index:%v\n",
				path_answer[path_idx], paths[path_idx].GetUUID(), path_idx)
			t.Fail()
			break
		}
	}
	t.Logf("Path cost: %v\n", cost)
	if cost != cost_answer {
		t.Logf("Path exempt cost does not match, expected/result: %v/%v\n", cost_answer, cost)
		t.Fail()
	}
}

/*
Testing the path exemption by means of finding the optimal paths in descending
order. Once all paths have been found and no more are attainable then
the algorithm will return nil paths.
Legend:
Cost between nodes: (#)
Node UUIDs: #
Network to be described and calculated:

       1---(1)--2--(1)--3
   (1)/                  \(1)
     / 4--(2)--5---(2)--6 \
    | /(2)            (2)\ |
    |/                    \|
    0--(3)7-(3)8-(3)-9-(3)-16
    |\                    /|
    | \(4)            (4)/ |
     \ 10-(4)-11--(4)-12  /
   (5)\                  /(5)
       13--(5)--14-(5)-15
*/
func TestExemptPaths(t *testing.T) {
	// Reminder: UUIDs do not need to be ordered, it's just done
	// here for readability purposes.
	node_json_data := []byte(`[
	{"UUID":0,"Neighbours_uuid":[1,4,7,10,13],"Neighbours_cost":[1,2,3,4,5]},
	{"UUID":1,"Neighbours_uuid":[2],"Neighbours_cost":[1]},
	{"UUID":2,"Neighbours_uuid":[3],"Neighbours_cost":[1]},
	{"UUID":3,"Neighbours_uuid":[16],"Neighbours_cost":[1]},
	{"UUID":4,"Neighbours_uuid":[5],"Neighbours_cost":[2]},
	{"UUID":5,"Neighbours_uuid":[6],"Neighbours_cost":[2]},
	{"UUID":6,"Neighbours_uuid":[16],"Neighbours_cost":[2]},
	{"UUID":7,"Neighbours_uuid":[8],"Neighbours_cost":[3]},
	{"UUID":8,"Neighbours_uuid":[9],"Neighbours_cost":[3]},
	{"UUID":9,"Neighbours_uuid":[16],"Neighbours_cost":[3]},
	{"UUID":10,"Neighbours_uuid":[11],"Neighbours_cost":[4]},
	{"UUID":11,"Neighbours_uuid":[12],"Neighbours_cost":[4]},
	{"UUID":12,"Neighbours_uuid":[16],"Neighbours_cost":[4]},
	{"UUID":13,"Neighbours_uuid":[14],"Neighbours_cost":[5]},
	{"UUID":14,"Neighbours_uuid":[15],"Neighbours_cost":[5]},
	{"UUID":15,"Neighbours_uuid":[16],"Neighbours_cost":[5]}
	]`)

	nodes := GenerateNodes(node_json_data)
	var exempt_paths map[UUID]*struct{} = make(map[UUID]*struct{})
	var path_answer []UUID = []UUID{3,2,1,6,5,4,9,8,7,12,11,10,15,14,13}
	var path_exempt_tally []UUID
	var path_answer_costs []Cost = []Cost{4,8,12,16,20}
	var path_exempt_costs []Cost

	// Calculate the best paths until there are no more attainable.
	paths, cost := CalculatePath(nodes[0], nodes[16], nil)
	for paths != nil {
		// Print the path using the UUIDs from destination to source.
		for path_idx := range paths {
			if !(paths[path_idx].GetUUID() == 0 || paths[path_idx].GetUUID() == 16) {
				exempt_paths[paths[path_idx].GetUUID()] = &struct{}{}
				path_exempt_tally = append(path_exempt_tally, paths[path_idx].GetUUID())
			}
			t.Logf("Path: %v\n", paths[path_idx].GetUUID())
		}
		// Print the final cost of the path.
		path_exempt_costs = append(path_exempt_costs, cost)
		t.Logf("Path cost: %v\n", cost)
		// Calculate another path, with the previous optimal path excluded
		paths, cost = CalculatePath(nodes[0], nodes[16], exempt_paths)
	}
	
	// Check that the paths are exempt in the correct order
	for path_idx := range path_answer {
		if path_answer[path_idx] != path_exempt_tally[path_idx] {
			t.Logf("Path does not match expected:\nWant:\t%v\nGot:\t%v\nAt index:%v\n",
				path_answer[path_idx], path_exempt_tally[path_idx], path_idx)
			t.Fail()
			break
		}
	}
	
	// Check that the costs are ascending in the correct order
	for path_idx := range path_answer_costs {
		if path_answer_costs[path_idx] != path_exempt_costs[path_idx] {
			t.Logf("Path cost does not match expected:\nWant:\t%v\nGot:\t%v\nAt index:%v\n",
				path_answer_costs[path_idx], path_exempt_costs[path_idx], path_idx)
			t.Fail()
			break
		}
	}
}
