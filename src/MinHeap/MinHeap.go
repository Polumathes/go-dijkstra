package MinHeap

import (
    "fmt"
)

type Element interface {
	GetValue() uint32
	GetData() interface{}
}

type MinHeap struct {
	elements []Element
}

func NewMinHeap() (*MinHeap) {
    var mh MinHeap
    return (&mh)
}

func getParentIndex(pos uint32) uint32 {
	if pos&1 > 0 {
		pos -= 1
	}

	if pos == 0 {
		return (0)
	}

	return (pos / 2)
}

func getChildIndex(pos uint32) (uint32, uint32) {
	return ((pos * 2) + 1), ((pos * 2) + 2)
}

func (h *MinHeap) GetElement(element_pos uint32) Element {
	heap_len := uint32(len(h.elements))

	if ((heap_len == 0) || (element_pos >= heap_len)) {
		return (nil)
	}

	return (h.elements[element_pos])
}

func (h *MinHeap) Clear() {
	h.elements = h.elements[:0]
}

func (h *MinHeap) GetLastIndex() uint32 {
	leaf_idx := uint32(len(h.elements))

	if leaf_idx > 0 {
		leaf_idx -= 1
	}

	return (leaf_idx)
}

func (h *MinHeap) swap(index_1 uint32, index_2 uint32) {
	if index_1 >= uint32(len(h.elements)) || index_2 >= uint32(len(h.elements)) {
		return
	}

	temp := h.elements[index_2]
	h.elements[index_2] = h.elements[index_1]
	h.elements[index_1] = temp
}

func (h *MinHeap) pop_sort() {
	parent_index := uint32(0)

	for parent_index < uint32(len(h.elements)) {
		first_child_index, second_child_index := getChildIndex(parent_index)
		parent_element := h.GetElement(parent_index)
		first_child_element := h.GetElement(first_child_index)
		second_child_element := h.GetElement(second_child_index)
		
		if (first_child_element == nil) {
		    //fmt.Printf("First child element is NULL. Index: %v\n", first_child_index)
		    return
		}

		min_index := first_child_index
		
		if second_child_element != nil && second_child_element.GetValue() < first_child_element.GetValue() {
			min_index = second_child_index
		}

		if parent_element.GetValue() > h.elements[min_index].GetValue() {
			h.swap(parent_index, min_index)
		} else {
		    // Nothing more to swap, no more sorting needed.
			return
		}

		parent_index = min_index
	}
}

func (h *MinHeap) Pop() Element {
	last_pos := len(h.elements)

	if last_pos > 0 {
		last_pos -= 1
	} else {
		return (nil)
	}

	root_element := h.elements[0]

	if last_pos == 0 {
		h.elements = h.elements[:len(h.elements)-1]
		return (root_element)
	}

	last_element := h.elements[last_pos]
	h.elements = h.elements[:len(h.elements)-1]
	h.elements[0] = last_element

	h.pop_sort()

	return (root_element)
}

func (h *MinHeap) Push(element Element) {
	if element == nil {
		return
	}

	h.elements = append(h.elements, element)

	current_index := h.GetLastIndex()
	parent_index := getParentIndex(current_index)
	current_element := element

	for current_element.GetValue() < h.elements[parent_index].GetValue() {
		current_element = h.elements[current_index]
		h.elements[current_index] = h.elements[parent_index]
		h.elements[parent_index] = current_element
		current_index = parent_index
		parent_index = getParentIndex(current_index)
	}
}

func (h *MinHeap) PushAll(element []Element) {
	for i := range element {
		h.Push(element[i])
	}
}

func (h *MinHeap) String() string {
	return (fmt.Sprintf("%v", h.elements))
}
