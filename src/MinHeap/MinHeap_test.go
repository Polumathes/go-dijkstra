package MinHeap

import (
    "testing"
)

type TestElement struct {
    value uint32
}

func (te TestElement) GetValue() uint32 {
    return (te.value)   
}

func (te TestElement) GetData() interface{} {
    return (nil)
}

func TestHeapPush(t *testing.T) {
    var heap MinHeap
    
    answer := []uint32{1, 2, 4, 3, 7, 6, 8, 5, 9}
    
    testElements := []TestElement{{5},{4},{6},{3},{7},{2},{8},{1},{9}}

    for i := range testElements {
        heap.Push(testElements[i])
    }
        
    if len(answer) != len(heap.elements) {
        t.FailNow()
    }
    
    for i:= range heap.elements {
        if answer[i] != heap.elements[i].GetValue() {
            t.Logf("Mismatch answer at index %v, values: %v/%v",
                i, answer[i], heap.elements[i].GetValue())
            t.Fail()
            break
        }
    }
    
    t.Logf("\nUnordered: %v\nOrdered: %v", testElements, heap)
}

func TestHeapPushPop(t *testing.T) {
    var heap MinHeap
    
    answer := []uint32{2, 3, 4, 5, 7, 6, 8, 9}
    
    testElements := []TestElement{{5},{4},{6},{3},{7},{2},{8},{1},{9}}

    for i := range testElements {
        heap.Push(testElements[i])
    }
    
    t.Logf("Before pop(): %v", heap.elements)
    
    _ = heap.Pop()
    
    t.Logf("After pop():  %v", heap.elements)
    
    for i:= range heap.elements {
        if answer[i] != heap.elements[i].GetValue() {
            t.Logf("Mismatch answer at index %v, expected/have: %v/%v",
                i, answer[i], heap.elements[i].GetValue())
            t.Fail()
            break
        }
    }
    
    t.Logf("\nUnordered: %v\nOrdered: %v", testElements, heap)
}

